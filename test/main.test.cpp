#define CATCH_CONFIG_MAIN
#include "./../src/main.h"
#include <catch2/catch.hpp>
#include <climits>


TEST_CASE("add-function work as designed","[mathemathic-operations]") {
	REQUIRE(add(3,4) == 7);
	REQUIRE(add(0,1) == 1);
	REQUIRE(add(0,0) == 0);
	REQUIRE(add(2,0) == 2);
	REQUIRE(add(-2, 1) == -1);
	REQUIRE_THROWS_AS( add(std::numeric_limits<int>::max(), 1), std::invalid_argument );
}

TEST_CASE("subtract-function work as designed","[mathemathic-operations]") {
	REQUIRE(subtract(3,2) == -1);
	REQUIRE(subtract(0,0) == 0);
	REQUIRE(subtract(0,3) == 3);
	REQUIRE(subtract(3,87) == 84);
}

TEST_CASE("multiply-function work as designed","[mathemathic-operations]") {
	REQUIRE(multiply(1,0) == 0);
	REQUIRE(multiply(2,2) == 4);
	REQUIRE(multiply(-3,4) == -12);
	REQUIRE(multiply(-3,-3) == 9);
	REQUIRE(multiply(2,0) == 0);
	REQUIRE_THROWS_AS( multiply(std::numeric_limits<int>::max(), 2), std::invalid_argument );
}

TEST_CASE("divide-function work as designed", "[mathemathic-operations]") {
	REQUIRE(divide(0,1) == 0);
	REQUIRE(divide(2,1) == 2);
	REQUIRE(divide(10,5) == 2);
	REQUIRE(divide(-2,1) == -2);
	REQUIRE(divide(6,-2) == -3);
	REQUIRE_THROWS_AS( divide(2,0), std::invalid_argument );
}

TEST_CASE("Evaluate Function for basic Functionality","[evaluate]") {
	REQUIRE( evaluate("23+") == 5 );	
	REQUIRE_THROWS_AS( evaluate("2%"), std::invalid_argument );
	REQUIRE_THROWS_AS( evaluate("2+"), std::invalid_argument );
	REQUIRE_THROWS_AS( evaluate("20/"), std::invalid_argument );
	REQUIRE_THROWS_AS( evaluate(""), std::invalid_argument );
}


