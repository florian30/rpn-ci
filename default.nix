with import <nixpkgs> {};

let
	rpn1 = derivation {
		make = "${pkgs.gnumake}/bin/make";
		src = ./.;
		mkdirpath = "${pkgs.coreutils}/bin/";
		compilerpath = "${pkgs.stdenv.cc}/bin/";
		
		name = "rpn1";
		builder	= "${pkgs.bash}/bin/bash";
		args = [ ./rpn1.sh ];
		system = builtins.currentSystem;
		};
	
	rpn2 = derivation {
		make = "${pkgs.gnumake}/bin/make";
		src = ./.;
		mkdirpath = "${pkgs.coreutils}/bin/";
		compilerpath = "${pkgs.stdenv.cc}/bin/";
		CXXFLAGS= "-fsanitize=undefined -fsanitize=address -I${pkgs.catch2}/include/";
	
		name = "rpn2";
		builder	= "${pkgs.bash}/bin/bash";
		args = [ ./rpn2.sh ];
		system = builtins.currentSystem;
	};
	
	rpn3 = derivation {
		make = "${pkgs.gnumake}/bin/make";
		src = ./.;
		mkdirpath = "${pkgs.coreutils}/bin/";
		compilerpath = "${pkgs.stdenv.cc}/bin/";
		aflpath = "${pkgs.afl}/bin/";
		
		name = "rpn3";
		builder	= "${pkgs.bash}/bin/bash";
		args = [ ./rpn3.sh ];
		system = builtins.currentSystem;	
	};
	
	rpn4 = deri: derivation {
		inherit deri;
		make = "${pkgs.gnumake}/bin/make";
		src = ./.;
		mkdirpath = "${pkgs.coreutils}/bin/";
		aflpath = "${pkgs.afl}/bin/";
		
		name = "rpn4";
		builder	= "${pkgs.bash}/bin/bash";
		args = [ ./rpn4.sh ];
		system = builtins.currentSystem;	
	};
	
in {
	rpn1-test = rpn1;
	rpn2-test = rpn2;
	rpn3-test = rpn3;
	rpn4-test = rpn4 rpn3;
}
