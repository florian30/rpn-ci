with import (import ./nixpkgs.nix) {};
stdenv.mkDerivation {
  name = "fuzzer-example";
  src = ./.;
  nativeBuildInputs = [ afl ];
  buildInputs = [ catch2 ];

  installPhase = ''
    mkdir -p $out
    find . -executable -type f -exec cp {} $out \;
  '';
}
