builtins.fetchTarball {
  url = "https://github.com/nixos/nixpkgs/archive/859ce47b023d80ccbcf446a112c813972a5949b6.tar.gz";
  sha256 = "14rmrpzbxbf8p4v70n39bvxm9qhnwlg4pjkzf7ya6n697g8yd1wx";
}
