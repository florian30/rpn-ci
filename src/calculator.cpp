#include "main.h"
#include <climits>
#include <iostream>
#include <string>

int main() {
  std::string user_input;
  std::cin >> user_input;

  if (user_input.empty()) {
    std::cout << "Empty input\n";
    return 1;
  }

  std::cout << "User input is \"" << user_input << "\"\n";

  int result = INT_MAX;

  try {
    result = evaluate(user_input);
  } catch (const std::exception &e) {
    std::cout << e.what();
  }
  if (result != INT_MAX) {
    std::cout << "Result is: " << result << '\n';
  }

}
