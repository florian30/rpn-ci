FROM ubuntu:latest

RUN apt-get -y update && \
	apt-get -y install make && \
    apt-get -y install afl && \
    apt-get -y install wget

COPY ./ /root/

WORKDIR /usr/include
RUN mkdir catch2

WORKDIR /usr/include/catch2
RUN wget --no-check-certificate "https://raw.githubusercontent.com/catchorg/Catch2/master/single_include/catch2/catch.hpp"

WORKDIR /root/
RUN make bin/tests

WORKDIR /root/bin
CMD ./tests

WORKDIR /root/
CMD make fuzz

