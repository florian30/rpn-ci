export PATH=$PATH:$mkdirpath:$aflpath

cp -r $src/* .

mkdir -p $out/out

afl-fuzz -i test/in -o $out/out -- $deri/bin/fuzz_main &
AFL_PID=$!

sleep 10
kill $AFL_PID
